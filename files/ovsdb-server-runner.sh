#!/usr/bin/env bash

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud/ for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

#
# This script tries to follow the styleguide from
# https://google.github.io/styleguide/shellguide.html
#

# -e Fail on all uncatched errors
# -u Fails when variables are used before initialization
# -o Uncatched errors must not be swallowed by pipes
set -euo pipefail

# Activate tracing if debugging variable is provided
if [[ "${DEBUG:-0}" -gt "0" ]]; then
  set -x
fi

rm -f /run/openvswitch/conf.db
ovsdb-tool create /run/openvswitch/conf.db

ovsdb-server /run/openvswitch/conf.db \
--pidfile=/run/openvswitch/ovsdb-server.pid \
--remote=punix:/run/openvswitch/db.sock \
-vconsole:off \
-vfile:info \
--log-file=/dev/stdout

