##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

ARG ovs_version
ARG make_parallelism=2

ARG LANG=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive

ARG BUILD_PACKAGES="autoconf automake build-essential git libtool"
ARG DEV_PACKAGES="libssl-dev libcap-ng-dev"
ARG REQUIRED_PACKAGES="iproute2"

ARG DESTDIR=/build

FROM python:3.13-slim-bullseye@sha256:9bf6829d24e9304305ec87973c3b73a94a347019ce0b21994eeb5101dba7c08e AS builder

ARG ovs_version
ARG make_parallelism

ARG LANG
ARG DEBIAN_FRONTEND

ARG BUILD_PACKAGES
ARG DEV_PACKAGES

ARG DESTDIR

RUN set -eux ; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${BUILD_PACKAGES} \
        ${DEV_PACKAGES}

RUN set -eux ; \
    git clone -b ${ovs_version} --depth 1 https://github.com/openvswitch/ovs /ovs; \
    cd /ovs; \
    ./boot.sh; \
    ./configure \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc; \
    gmake -j ${make_parallelism}; \
    make -j ${make_parallelism} install DESTDIR=${DESTDIR}

FROM python:3.13-slim-bullseye@sha256:9bf6829d24e9304305ec87973c3b73a94a347019ce0b21994eeb5101dba7c08e

ARG LANG
ARG DEBIAN_FRONTEND

ARG REQUIRED_PACKAGES

ARG DESTDIR

COPY --from=builder ${DESTDIR}/ /
COPY --chown=root files/* /

RUN set -eux ; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${REQUIRED_PACKAGES}; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*; \
    ovs-appctl --version
